public class Car {
	private int distance;
	private String color;
	
	//initialize the car with default values
	public Car() {
		distance = 0;
		color = "black";
	}
	
	//initialize the car with a specified color
	public Car(String sentColor) {
		distance = 0;
		color = sentColor;
	}
	
	//speed is in m/h and time is in hours
	public void drive (int speed, int time) {
		distance = speed * time;
		System.out.println("The " + color + " car is now " + distance + " miles down the road.");
	}
	
}
