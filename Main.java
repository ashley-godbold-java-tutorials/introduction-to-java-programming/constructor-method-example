public class Main {
	public static void main(String []args){
		Car blackCar = new Car();
		Car redCar = new Car("red");
		Car blueCar = new Car("blue");
		
		blackCar.drive(36, 2);
		redCar.drive(60, 1);
		blueCar.drive(45,  4);
	
	}
}